<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_MX">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Conversación</translation>
    </message>
    <message>
        <source>Cenzor</source>
        <translation>Censor</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Enable cenzor</source>
        <translation>Habilitar censor</translation>
    </message>
    <message>
        <source>Admonition</source>
        <translation>Amonestación</translation>
    </message>
    <message>
        <source>Swearwords</source>
        <translation>Malas palabras</translation>
    </message>
    <message>
        <source>Exclusions</source>
        <translation>Exclusiones</translation>
    </message>
    <message>
        <source>Message was cenzored</source>
        <translation>El mensaje fue censurado</translation>
    </message>
</context>
<context>
    <name>CenzorNotification</name>
    <message>
        <source>Message was cenzored</source>
        <translation>El mensaje fue censurado</translation>
    </message>
    <message>
        <source>Your interlocutor used obscene word and became admonished</source>
        <translation>Su interlocultor usó palabra obscena y fue amonestado</translation>
    </message>
    <message>
        <source>Cenzor</source>
        <translation>Censor</translation>
    </message>
</context>
<context>
    <name>ListEditWidget</name>
    <message>
        <source>Add</source>
        <translation>Agregar</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Cambiar</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
</context>
</TS>
