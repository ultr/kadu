<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl">
<context>
    <name>@default</name>
    <message>
        <source>Tray</source>
        <translation>Tacka systemowa</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Blinking Envelope</source>
        <translation>Mrugająca</translation>
    </message>
    <message>
        <source>Static Envelope</source>
        <translation>Statyczna</translation>
    </message>
    <message>
        <source>Animated Envelope</source>
        <translation>Animowana</translation>
    </message>
    <message>
        <source>Startup</source>
        <translation>Uruchamianie</translation>
    </message>
    <message>
        <source>Start minimized</source>
        <translation>Uruchom zminimalizowany</translation>
    </message>
    <message>
        <source>Show tooltip over tray icon</source>
        <translation>Pokaż podpowiedź w tacce systemowej</translation>
    </message>
    <message>
        <source>Tray icon indicating new message</source>
        <translation>Zachowanie ikony w tacce systemowej po otrzymaniu nowej wiadomości</translation>
    </message>
</context>
<context>
    <name>DockingManager</name>
    <message>
        <source>&amp;Restore</source>
        <translation>P&amp;rzywróć</translation>
    </message>
    <message>
        <source>&amp;Minimize</source>
        <translation>&amp;Minimalizuj</translation>
    </message>
    <message>
        <source>&amp;Exit Kadu</source>
        <translation>&amp;Zakończ Kadu</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Opis</translation>
    </message>
    <message>
        <source>Statuses</source>
        <translation>Statusy</translation>
    </message>
    <message>
        <source>Descriptions</source>
        <translation>Opisy</translation>
    </message>
    <message>
        <source>Silent mode</source>
        <translation>Tryb cichy</translation>
    </message>
</context>
</TS>
