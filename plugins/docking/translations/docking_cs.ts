<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>@default</name>
    <message>
        <source>Tray</source>
        <translation>Oznamovací oblast panelu</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <source>Blinking Envelope</source>
        <translation>Blikající obálka</translation>
    </message>
    <message>
        <source>Static Envelope</source>
        <translation>Nehybná obálka</translation>
    </message>
    <message>
        <source>Animated Envelope</source>
        <translation>Pohybující se obálka</translation>
    </message>
    <message>
        <source>Startup</source>
        <translation>Spuštění</translation>
    </message>
    <message>
        <source>Start minimized</source>
        <translation>Spustit zmenšený</translation>
    </message>
    <message>
        <source>Show tooltip over tray icon</source>
        <translation>Ukázat nástrojovou radu nad ikonou v oznamovací oblasti panelu</translation>
    </message>
    <message>
        <source>Tray icon indicating new message</source>
        <translation>Ikona v oznamovací oblasti panelu oznamující novou zprávu</translation>
    </message>
</context>
<context>
    <name>DockingManager</name>
    <message>
        <source>&amp;Restore</source>
        <translation>&amp;Obnovit</translation>
    </message>
    <message>
        <source>&amp;Minimize</source>
        <translation>&amp;Zmenšit</translation>
    </message>
    <message>
        <source>&amp;Exit Kadu</source>
        <translation>&amp;Ukončit Kadu</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Stav</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Popis</translation>
    </message>
    <message>
        <source>Statuses</source>
        <translation>Stavy</translation>
    </message>
    <message>
        <source>Descriptions</source>
        <translation>Popisy</translation>
    </message>
    <message>
        <source>Silent mode</source>
        <translation>Tichý režim</translation>
    </message>
</context>
</TS>
