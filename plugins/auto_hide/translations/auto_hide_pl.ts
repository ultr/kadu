<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl">
<context>
    <name>@default</name>
    <message>
        <source>Autohide idle time</source>
        <translation>Czas bezczynności</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n sekunda</numerusform>
            <numerusform>%n sekundy</numerusform>
            <numerusform>%n sekund</numerusform>
        </translation>
    </message>
    <message>
        <source>Buddies list</source>
        <translation>Lista znajomych</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Buddies window</source>
        <translation>Okno znajomych</translation>
    </message>
    <message>
        <source>Hide buddy list window when unused</source>
        <translation>Ukryj okno znajomych, kiedy nie jest używane</translation>
    </message>
</context>
<context>
    <name>AutoHide</name>
    <message>
        <source>Don&apos;t hide</source>
        <translation>Nie ukrywaj</translation>
    </message>
</context>
</TS>
