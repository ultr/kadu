<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>@default</name>
    <message>
        <source>Autohide idle time</source>
        <translation>Temps d&apos;inactivité avant de déclenchement d&apos;Autohide </translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n seconde</numerusform>
            <numerusform>%n secondes</numerusform>
        </translation>
    </message>
    <message>
        <source>Buddies list</source>
        <translation>Liste de contacts</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Buddies window</source>
        <translation>Fenêtre de contacts</translation>
    </message>
    <message>
        <source>Hide buddy list window when unused</source>
        <translation>Masquer la fenêtre de contact lorsque non utilisé</translation>
    </message>
</context>
<context>
    <name>AutoHide</name>
    <message>
        <source>Don&apos;t hide</source>
        <translation>Ne pas masquer</translation>
    </message>
</context>
</TS>
