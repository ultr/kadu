<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>@default</name>
    <message>
        <source>Autohide idle time</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n segundos</numerusform>
            <numerusform>%n segundo</numerusform>
        </translation>
    </message>
    <message>
        <source>Buddies list</source>
        <translation>Lista de contatos</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Geral</translation>
    </message>
    <message>
        <source>Buddies window</source>
        <translation>Janela de contatos</translation>
    </message>
    <message>
        <source>Hide buddy list window when unused</source>
        <translation>Esconder janela da lista de contatos quando não estiver em uso</translation>
    </message>
</context>
<context>
    <name>AutoHide</name>
    <message>
        <source>Don&apos;t hide</source>
        <translation>Não esconder</translation>
    </message>
</context>
</TS>
