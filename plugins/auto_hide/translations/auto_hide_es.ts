<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>@default</name>
    <message>
        <source>Autohide idle time</source>
        <translation>Tiempo de espera para Auto-Ocultar</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n segundos</numerusform>
            <numerusform>%n segundos</numerusform>
        </translation>
    </message>
    <message>
        <source>Buddies list</source>
        <translation>Lista de Amigos</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Buddies window</source>
        <translation>Ventana de Amigos</translation>
    </message>
    <message>
        <source>Hide buddy list window when unused</source>
        <translation>Ocultar la ventana lista de contactos cuando no se utilice</translation>
    </message>
</context>
<context>
    <name>AutoHide</name>
    <message>
        <source>Don&apos;t hide</source>
        <translation>No ocultar</translation>
    </message>
</context>
</TS>
