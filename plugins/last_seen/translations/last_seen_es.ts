<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>Infos</name>
    <message>
        <source>&amp;Show infos about buddies</source>
        <translation>&amp;Mostrar información sobre los amigos</translation>
    </message>
</context>
<context>
    <name>InfosDialog</name>
    <message>
        <source>Buddies Information</source>
        <translation>Información de los amigos</translation>
    </message>
    <message>
        <source>Buddy</source>
        <translation>Amigo</translation>
    </message>
    <message>
        <source>Protocol</source>
        <translation>Protocolo</translation>
    </message>
    <message>
        <source>Nick</source>
        <translation>Apodo</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <source>State</source>
        <translation>Estado</translation>
    </message>
    <message>
        <source>Last time seen on</source>
        <translation>Última vez visto</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Nombre de usuario</translation>
    </message>
</context>
</TS>
