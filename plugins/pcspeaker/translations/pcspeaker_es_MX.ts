<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_MX">
<context>
    <name>@default</name>
    <message>
        <source>PC Speaker</source>
        <translation>Bocina de la PC</translation>
    </message>
</context>
<context>
    <name>PCSpeakerConfigurationWidget</name>
    <message>
        <source>Put the played sounds separate by space, _ for pause, eg. D2 C1# G0</source>
        <translation>Ponga los sonidos reproducidos separados por un espacio, _ para una pausa, ejemplo. D2 C1# G0</translation>
    </message>
</context>
</TS>
