<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>@default</name>
    <message>
        <source>Notifications</source>
        <translation>Notificações</translation>
    </message>
    <message>
        <source>Sound</source>
        <translation>Som</translation>
    </message>
    <message>
        <source>Sound player</source>
        <translation>Reprodutor de audio</translation>
    </message>
    <message>
        <source>Player</source>
        <translation>Reprodutor</translation>
    </message>
</context>
</TS>
