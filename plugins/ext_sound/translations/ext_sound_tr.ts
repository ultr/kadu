<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>@default</name>
    <message>
        <source>Notifications</source>
        <translation>Uyarıclar</translation>
    </message>
    <message>
        <source>Sound</source>
        <translation>Ses</translation>
    </message>
    <message>
        <source>Sound player</source>
        <translation>Ses oynatıcı</translation>
    </message>
    <message>
        <source>Player</source>
        <translation>Oynatıcı</translation>
    </message>
</context>
</TS>
