<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>@default</name>
    <message>
        <source>Notifications</source>
        <translation>Oznámení</translation>
    </message>
    <message>
        <source>Sound</source>
        <translation>Zvuk</translation>
    </message>
    <message>
        <source>Sound player</source>
        <translation>Přehrávač zvuku</translation>
    </message>
    <message>
        <source>Player</source>
        <translation>Přehrávač</translation>
    </message>
</context>
</TS>
