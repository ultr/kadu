<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>チャット</translation>
    </message>
    <message>
        <source>Antistring</source>
        <translation>アンチストリング</translation>
    </message>
    <message>
        <source>General</source>
        <translation>概要</translation>
    </message>
    <message>
        <source>Enable Antistring</source>
        <translation>アンチストリング有効化</translation>
    </message>
    <message>
        <source>Block message</source>
        <translation>メッセージブロック</translation>
    </message>
    <message>
        <source>Admonition</source>
        <translation>警告</translation>
    </message>
    <message>
        <source>Log</source>
        <translation>ログ</translation>
    </message>
    <message>
        <source>Write log to file</source>
        <translation>ログをファイルに保存</translation>
    </message>
    <message>
        <source>Conditions</source>
        <translation>状態</translation>
    </message>
    <message>
        <source>Antistring notifications</source>
        <translation>アンチストリングの通知</translation>
    </message>
</context>
<context>
    <name>Antistring</name>
    <message>
        <source>     DATA AND TIME      ::   ID   ::    MESSAGE
</source>
        <translation>日付と時間 :: ID :: メッセージ
</translation>
    </message>
</context>
<context>
    <name>AntistringConfigurationUiHandler</name>
    <message>
        <source>Condition</source>
        <translation>状態</translation>
    </message>
    <message>
        <source>Don&apos;t use</source>
        <translation>使用不可</translation>
    </message>
    <message>
        <source>Factor</source>
        <translation>原因</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>追加</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>変更</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>削除</translation>
    </message>
</context>
<context>
    <name>AntistringNotification</name>
    <message>
        <source>Antistring</source>
        <translation>アンチストリング</translation>
    </message>
    <message>
        <source>Your interlocutor send you love letter</source>
        <translation>対談者に愛を送信</translation>
    </message>
</context>
</TS>
