<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Rozhovor</translation>
    </message>
    <message>
        <source>Antistring</source>
        <translation>Systém protisněhové koule</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <source>Enable Antistring</source>
        <translation>Povolit systém protisněhové koule</translation>
    </message>
    <message>
        <source>Block message</source>
        <translation>Blokovat zprávu</translation>
    </message>
    <message>
        <source>Admonition</source>
        <translation>Napomenutí</translation>
    </message>
    <message>
        <source>Log</source>
        <translation>Zápis</translation>
    </message>
    <message>
        <source>Write log to file</source>
        <translation>Zapsat zápis do souboru</translation>
    </message>
    <message>
        <source>Conditions</source>
        <translation>Podmínky</translation>
    </message>
    <message>
        <source>Antistring notifications</source>
        <translation>Oznámení protisněhové koule</translation>
    </message>
</context>
<context>
    <name>Antistring</name>
    <message>
        <source>     DATA AND TIME      ::   ID   ::    MESSAGE
</source>
        <translation>     DATUM A ČAS      ::   ID   ::    ZPRÁVA
</translation>
    </message>
</context>
<context>
    <name>AntistringConfigurationUiHandler</name>
    <message>
        <source>Condition</source>
        <translation>Podmínka</translation>
    </message>
    <message>
        <source>Don&apos;t use</source>
        <translation>Nepoužívat</translation>
    </message>
    <message>
        <source>Factor</source>
        <translation>Faktor</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Změnit</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
</context>
<context>
    <name>AntistringNotification</name>
    <message>
        <source>Antistring</source>
        <translation>Systém protisněhové koule</translation>
    </message>
    <message>
        <source>Your interlocutor send you love letter</source>
        <translation>Účastník vaší rozmluvy vám poslal zamilovaný dopis</translation>
    </message>
</context>
</TS>
