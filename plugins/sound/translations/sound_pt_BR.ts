<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>@default</name>
    <message>
        <source>Sound</source>
        <translation>Som</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Notificações</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Geral</translation>
    </message>
    <message>
        <source>Sound theme</source>
        <translation>Tema de sons</translation>
    </message>
    <message>
        <source>Sound paths</source>
        <translation>Caminhos de sons</translation>
    </message>
    <message>
        <source>Test sound playing</source>
        <translation>Testar reprodução de sons</translation>
    </message>
    <message>
        <source>Play a sound</source>
        <translation>Reproduzir um som</translation>
    </message>
    <message>
        <source>Enable sound notifications</source>
        <translation>Habilitar notificações sonoras</translation>
    </message>
</context>
<context>
    <name>SoundBuddyConfigurationWidget</name>
    <message>
        <source>Use custom sound</source>
        <translation>Usar sons padrão</translation>
    </message>
    <message>
        <source>Sound</source>
        <translation>Som</translation>
    </message>
</context>
<context>
    <name>SoundChatConfigurationWidget</name>
    <message>
        <source>Sound</source>
        <translation>Som</translation>
    </message>
    <message>
        <source>Use custom sound</source>
        <translation>Usar sons padrão</translation>
    </message>
</context>
<context>
    <name>SoundConfigurationUiHandler</name>
    <message>
        <source>Custom</source>
        <translation>Padrão</translation>
    </message>
</context>
<context>
    <name>SoundMuteAction</name>
    <message>
        <source>Play Sounds</source>
        <translation>Reproduzir sons</translation>
    </message>
</context>
</TS>
