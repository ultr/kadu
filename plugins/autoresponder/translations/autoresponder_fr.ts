<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Conversation</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>Only for the first time</source>
        <translation>Seulement la première fois</translation>
    </message>
    <message>
        <source>Respond to conferences</source>
        <translation>Réponde à des conférences</translation>
    </message>
    <message>
        <source>Status invisible</source>
        <translation>État invisible</translation>
    </message>
    <message>
        <source>Status busy</source>
        <translation>État occupé</translation>
    </message>
    <message>
        <source>Status available</source>
        <translation>État disponible</translation>
    </message>
    <message>
        <source>Auto Responder</source>
        <translation>Répondeur automatique</translation>
    </message>
    <message>
        <source>Auto answer text</source>
        <translation>Message de réponse automatique</translation>
    </message>
    <message>
        <source>Choose Status</source>
        <translation>Choisir l&apos;état</translation>
    </message>
</context>
<context>
    <name>AutoResponder</name>
    <message>
        <source>KADU AUTORESPONDER:</source>
        <translation>RÉPONDEUR AUTOMATIQUE KADU :</translation>
    </message>
</context>
<context>
    <name>AutoresponderConfigurator</name>
    <message>
        <source>I am busy.</source>
        <translation>Je suis occupé.</translation>
    </message>
</context>
</TS>
