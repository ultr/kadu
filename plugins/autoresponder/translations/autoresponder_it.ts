<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Opzioni</translation>
    </message>
    <message>
        <source>Only for the first time</source>
        <translation>Solo per la prima volta</translation>
    </message>
    <message>
        <source>Respond to conferences</source>
        <translation>Rispondi alle conferenze</translation>
    </message>
    <message>
        <source>Status invisible</source>
        <translation>Stato Invisibile</translation>
    </message>
    <message>
        <source>Status busy</source>
        <translation>Occupato</translation>
    </message>
    <message>
        <source>Status available</source>
        <translation>Stato Disponibile</translation>
    </message>
    <message>
        <source>Auto Responder</source>
        <translation>Risposta automatica</translation>
    </message>
    <message>
        <source>Auto answer text</source>
        <translation>Auto risposta</translation>
    </message>
    <message>
        <source>Choose Status</source>
        <translation>Scegli lo stato</translation>
    </message>
</context>
<context>
    <name>AutoResponder</name>
    <message>
        <source>KADU AUTORESPONDER:</source>
        <translation>RISPOSTA AUTOMATICA:</translation>
    </message>
</context>
<context>
    <name>AutoresponderConfigurator</name>
    <message>
        <source>I am busy.</source>
        <translation>Sono occupato.</translation>
    </message>
</context>
</TS>
