<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Konuşma</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Seçenekler</translation>
    </message>
    <message>
        <source>Only for the first time</source>
        <translation>Sadece ilk sefer için</translation>
    </message>
    <message>
        <source>Respond to conferences</source>
        <translation>Konferansları yanıtla</translation>
    </message>
    <message>
        <source>Status invisible</source>
        <translation>Durum görünmez</translation>
    </message>
    <message>
        <source>Status busy</source>
        <translation>Durum meşgul</translation>
    </message>
    <message>
        <source>Status available</source>
        <translation>Durum müsait</translation>
    </message>
    <message>
        <source>Auto Responder</source>
        <translation>Otomatik Yanıtlayıcı</translation>
    </message>
    <message>
        <source>Auto answer text</source>
        <translation>Otomatik yanıt yazısı</translation>
    </message>
    <message>
        <source>Choose Status</source>
        <translation>Durum Seçiniz</translation>
    </message>
</context>
<context>
    <name>AutoResponder</name>
    <message>
        <source>KADU AUTORESPONDER:</source>
        <translation>KADU OTO-YANITLAYICI:</translation>
    </message>
</context>
<context>
    <name>AutoresponderConfigurator</name>
    <message>
        <source>I am busy.</source>
        <translation>Meşgulüm.</translation>
    </message>
</context>
</TS>
