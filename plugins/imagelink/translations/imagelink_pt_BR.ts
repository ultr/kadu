<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Conversa</translation>
    </message>
    <message>
        <source>Show YouTube movies in chat window</source>
        <translation>Mostrar vídeos do YouTube na janela de conversa</translation>
    </message>
    <message>
        <source>Show images in chat window</source>
        <translation>Mostrar imagens na janela de conversa</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Geral</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Mensagens</translation>
    </message>
</context>
</TS>
