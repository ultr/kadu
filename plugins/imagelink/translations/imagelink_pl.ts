<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Rozmowa</translation>
    </message>
    <message>
        <source>Show YouTube movies in chat window</source>
        <translation>Pokaż filmy z YouTube w oknie rozmowy</translation>
    </message>
    <message>
        <source>Show images in chat window</source>
        <translation>Pokaż obrazki w oknie rozmowy</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Wiadomości</translation>
    </message>
</context>
</TS>
