<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Conversación</translation>
    </message>
    <message>
        <source>Show YouTube movies in chat window</source>
        <translation>Mostrar vídeos de YouTube en la ventana de conversación</translation>
    </message>
    <message>
        <source>Show images in chat window</source>
        <translation>Mostrar Imágenes en la ventana de conversación</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Messages</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
