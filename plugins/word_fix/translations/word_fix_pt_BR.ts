<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Conversa</translation>
    </message>
    <message>
        <source>Words fix</source>
        <translation>Correção de palavras</translation>
    </message>
    <message>
        <source>Enable word fix</source>
        <translation>Habilitar correção de palavras</translation>
    </message>
    <message>
        <source>Spelling</source>
        <translation>Ortografia</translation>
    </message>
</context>
<context>
    <name>WordFix</name>
    <message>
        <source>A word to be replaced</source>
        <translation>Uma palavra para ser substituída</translation>
    </message>
    <message>
        <source>Value to replace with</source>
        <translation>Substituir com</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Adicionar</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Mudar</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Apagar</translation>
    </message>
    <message>
        <source>Word</source>
        <translation>Palavra</translation>
    </message>
    <message>
        <source>Replace with</source>
        <translation>Substituir com</translation>
    </message>
</context>
</TS>
