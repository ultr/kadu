<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>チャット</translation>
    </message>
    <message>
        <source>Words fix</source>
        <translation>文字修正</translation>
    </message>
    <message>
        <source>Enable word fix</source>
        <translation>文字修正を有効化</translation>
    </message>
    <message>
        <source>Spelling</source>
        <translation>スペルチェック</translation>
    </message>
</context>
<context>
    <name>WordFix</name>
    <message>
        <source>A word to be replaced</source>
        <translation>単語を置き換えます</translation>
    </message>
    <message>
        <source>Value to replace with</source>
        <translation>置き換えられる単語</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>追加</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>変更</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>削除</translation>
    </message>
    <message>
        <source>Word</source>
        <translation>単語</translation>
    </message>
    <message>
        <source>Replace with</source>
        <translation>置き換える単語</translation>
    </message>
</context>
</TS>
