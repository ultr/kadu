<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Чат</translation>
    </message>
    <message>
        <source>Words fix</source>
        <translation>Коррекция слов</translation>
    </message>
    <message>
        <source>Enable word fix</source>
        <translation>Включить коррекцию слов</translation>
    </message>
    <message>
        <source>Spelling</source>
        <translation>Произношение</translation>
    </message>
</context>
<context>
    <name>WordFix</name>
    <message>
        <source>A word to be replaced</source>
        <translation>Слово для замены</translation>
    </message>
    <message>
        <source>Value to replace with</source>
        <translation>Изменяемое значение</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Изменить</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Word</source>
        <translation>слово</translation>
    </message>
    <message>
        <source>Replace with</source>
        <translation>Заменить на</translation>
    </message>
</context>
</TS>
