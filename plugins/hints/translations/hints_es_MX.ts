<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_MX">
<context>
    <name>@default</name>
    <message>
        <source>Hints</source>
        <translation>Indicaciones</translation>
    </message>
    <message>
        <source>Look</source>
        <translation>Apariencia</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Notificaciones</translation>
    </message>
    <message>
        <source>Layout</source>
        <translation>Disposición</translation>
    </message>
    <message>
        <source>New hints go</source>
        <translation>Nuevas indicaciones van</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation>Automático</translation>
    </message>
    <message>
        <source>Own hints position</source>
        <translation>Posición propia de indicaciones</translation>
    </message>
    <message>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <source>Minimum width</source>
        <translation>Ancho mínimo</translation>
    </message>
    <message>
        <source>Maximum width</source>
        <translation>Ancho máximo</translation>
    </message>
    <message>
        <source>Hints position preview</source>
        <translation>Vista previa de la posición de indicaciones</translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation>Tiempo de espera</translation>
    </message>
    <message>
        <source>Font color</source>
        <translation>Color del tipo de letra</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Color de fondo</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>Tipo de letra</translation>
    </message>
    <message>
        <source>Left button</source>
        <translation>Botón izquierdo</translation>
    </message>
    <message>
        <source>Nothing</source>
        <translation>Nada</translation>
    </message>
    <message>
        <source>Middle button</source>
        <translation>Botón central</translation>
    </message>
    <message>
        <source>Right button</source>
        <translation>Botón derecho</translation>
    </message>
    <message>
        <source>&apos;Open chat&apos; works on all events</source>
        <translation>&apos;Abrir conversación&apos; funciona en todos los eventos</translation>
    </message>
    <message>
        <source>Show message content in hint</source>
        <translation>Mostrar el contenido del mensaje en la indicación</translation>
    </message>
    <message>
        <source>Number of quoted characters</source>
        <translation>Numero de caracteres entre comillas</translation>
    </message>
    <message>
        <source>Delete pending message when user deletes hint</source>
        <translation>Borrar mensaje pendiente cuando el usuario elimina la indicación</translation>
    </message>
    <message>
        <source>Buddy List</source>
        <translation>Lista de compañeros</translation>
    </message>
    <message>
        <source>Hint Over Buddy</source>
        <translation>Indicación sobre el compañero</translation>
    </message>
    <message>
        <source>Border color</source>
        <translation>Color del borde</translation>
    </message>
    <message>
        <source>Border width</source>
        <translation>Ancho del borde</translation>
    </message>
    <message>
        <source>Transparency</source>
        <translation>Transparencia</translation>
    </message>
    <message>
        <source>Mouse Buttons</source>
        <translation>Botones del Ratón</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Sintaxis</translation>
    </message>
    <message>
        <source>Icon size</source>
        <translation>Tamaño de icono</translation>
    </message>
    <message>
        <source>16px</source>
        <translation>16px</translation>
    </message>
    <message>
        <source>22px</source>
        <translation>22px</translation>
    </message>
    <message>
        <source>32px</source>
        <translation>32px</translation>
    </message>
    <message>
        <source>48px</source>
        <translation>48px</translation>
    </message>
    <message>
        <source>64px</source>
        <translation>64px</translation>
    </message>
    <message>
        <source>New Chat/Message</source>
        <translation>Nuevo Conversación/Mensaje</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Avanzado</translation>
    </message>
    <message>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n segundo</numerusform>
            <numerusform>%n segundos</numerusform>
        </translation>
    </message>
    <message>
        <source>Margin size</source>
        <translation>Tamaño del margen</translation>
    </message>
    <message>
        <source>Hint&apos;s corner</source>
        <translation>Esquina de la indicación</translation>
    </message>
    <message>
        <source>On Top</source>
        <translation>En la parte superior</translation>
    </message>
    <message>
        <source>On Bottom</source>
        <translation>En la parte inferior</translation>
    </message>
    <message>
        <source>Top Left</source>
        <translation>Arriba a la Izquierda</translation>
    </message>
    <message>
        <source>Top Right</source>
        <translation>Arriba a la Derecha</translation>
    </message>
    <message>
        <source>Bottom Left</source>
        <translation>Abajo a la Izquierda</translation>
    </message>
    <message>
        <source>Bottom Right</source>
        <translation>Abajo a la Derecha</translation>
    </message>
    <message>
        <source>Open Chat</source>
        <translation>Abrir Conversación</translation>
    </message>
    <message>
        <source>Delete Hint</source>
        <translation>Borrar Indicación</translation>
    </message>
    <message>
        <source>Delete All Hints</source>
        <translation>Borrar Todas las Indicaciones</translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation>&lt;b&gt;Aquí&lt;/b&gt; puede ver la vista previa</translation>
    </message>
    <message>
        <source>Show buttons only if notification requires user&apos;s action</source>
        <translation>Mostrar botones sólo si la notificación requiere la acción del usuario</translation>
    </message>
    <message>
        <source>Hints size and position...</source>
        <translation>Posición y Tamaño de Indicaciones...</translation>
    </message>
</context>
<context>
    <name>HintManager</name>
    <message>
        <source>&lt;table&gt;&lt;tr&gt;&lt;td align=&quot;left&quot; valign=&quot;top&quot;&gt;&lt;img style=&quot;max-width:64px; max-height:64px;&quot; src=&quot;{#{avatarPath} #{avatarPath}}{~#{avatarPath} @{kadu_icons/kadu:64x64}}&quot;&gt;&lt;/td&gt;&lt;td width=&quot;100%&quot;&gt;&lt;div&gt;[&lt;b&gt;%a&lt;/b&gt;][&amp;nbsp;&lt;b&gt;(%g)&lt;/b&gt;]&lt;/div&gt;[&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;#{statusIconPath}&quot;&gt;&amp;nbsp;&amp;nbsp;%u&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;@{phone:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%m&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;@{mail-message-new:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%e&lt;/div&gt;]&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;[&lt;hr&gt;&lt;b&gt;%s&lt;/b&gt;][&lt;b&gt;:&lt;/b&gt;&lt;br&gt;&lt;small&gt;%d&lt;/small&gt;]</source>
        <translation>&lt;table&gt;&lt;tr&gt;&lt;td align=&quot;left&quot; valign=&quot;top&quot;&gt;&lt;img style=&quot;max-width:64px; max-height:64px;&quot; src=&quot;{#{avatarPath} #{avatarPath}}{~#{avatarPath} @{kadu_icons/kadu:64x64}}&quot;&gt;&lt;/td&gt;&lt;td width=&quot;100%&quot;&gt;&lt;div&gt;[&lt;b&gt;%a&lt;/b&gt;][&amp;nbsp;&lt;b&gt;(%g)&lt;/b&gt;]&lt;/div&gt;[&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;#{statusIconPath}&quot;&gt;&amp;nbsp;&amp;nbsp;%u&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;@{phone:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%m&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;@{mail-message-new:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%e&lt;/div&gt;]&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;[&lt;hr&gt;&lt;b&gt;%s&lt;/b&gt;][&lt;b&gt;:&lt;/b&gt;&lt;br&gt;&lt;small&gt;%d&lt;/small&gt;]</translation>
    </message>
</context>
<context>
    <name>HintOverUserConfigurationWindow</name>
    <message>
        <source>Hint Over Buddy Configuration</source>
        <translation>Indicación sobre Configuración de Compañero</translation>
    </message>
    <message>
        <source>Update preview</source>
        <translation>Actualizar vista previa</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Sintaxis</translation>
    </message>
</context>
<context>
    <name>HintsConfigurationUiHandler</name>
    <message>
        <source>Configure</source>
        <translation>Configurar</translation>
    </message>
    <message>
        <source>Hint over buddy list: </source>
        <translation>Indicación sobre la lista de compañeros: </translation>
    </message>
    <message>
        <source>Advanced hints&apos; configuration</source>
        <translation>Configuración avanzada de indicaciones</translation>
    </message>
</context>
<context>
    <name>HintsConfigurationWidget</name>
    <message>
        <source>Configure</source>
        <translation>Configurar</translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation>&lt;b&gt;Aquí&lt;/b&gt; puede ver la vista previa</translation>
    </message>
</context>
<context>
    <name>HintsConfigurationWindow</name>
    <message>
        <source>Hints configuration</source>
        <translation>Configuración de indicaciones</translation>
    </message>
    <message>
        <source>Don&apos;t hide</source>
        <translation>No ocultar</translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation>&lt;b&gt;Aquí&lt;/b&gt; puede ver la vista previa</translation>
    </message>
</context>
</TS>
