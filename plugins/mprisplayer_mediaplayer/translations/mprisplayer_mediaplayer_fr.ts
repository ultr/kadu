<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>@default</name>
    <message>
        <source>MediaPlayer</source>
        <translation>MediaPlayer</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>MPRIS Player</source>
        <translation>MPRIS Player</translation>
    </message>
</context>
<context>
    <name>MPRISPlayerConfigurationUiHandler</name>
    <message>
        <source>Select Player:</source>
        <translation>Choisir un lecteur :</translation>
    </message>
    <message>
        <source>Add Player</source>
        <translation>Ajouter un lecteur</translation>
    </message>
    <message>
        <source>Edit Player</source>
        <translation>Modifier un lecteur</translation>
    </message>
    <message>
        <source>Delete Player</source>
        <translation>Supprimer un lecteur</translation>
    </message>
</context>
<context>
    <name>MPRISPlayerDialog</name>
    <message>
        <source>Add Player</source>
        <translation>Ajouter un lecteur</translation>
    </message>
    <message>
        <source>Edit Player</source>
        <translation>Ajouter un lecteur</translation>
    </message>
    <message>
        <source>Player:</source>
        <translation>Lecteur :</translation>
    </message>
    <message>
        <source>Service:</source>
        <translation>Service :</translation>
    </message>
</context>
</TS>
