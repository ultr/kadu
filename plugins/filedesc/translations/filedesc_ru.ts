<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>@default</name>
    <message>
        <source>FileDesc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File with description to synchronize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter here a file, which will contain descriptions to refresh by module.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If you choose status without description, module will set it automatically to similar but with description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Allow other descriptions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Allows you to set some custom description manualy, until file contents does not change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Behavior</source>
        <translation>Поведение</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
    <message>
        <source>Always add description to status</source>
        <translation>Всегда добавлять описание к статусу</translation>
    </message>
</context>
</TS>
